call plug#begin()
Plug 'neovim/nvim-lspconfig'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'Shougo/deoplete-lsp'
Plug 'ervandew/supertab'
Plug 'tpope/vim-fugitive'
Plug 'vim-airline/vim-airline'
Plug 'scrooloose/nerdtree'
Plug 'xuyuanp/nerdtree-git-plugin'
Plug 'pest-parser/pest.vim'
Plug 'leafgarland/typescript-vim'
Plug 'peitalin/vim-jsx-typescript'
Plug 'airblade/vim-gitgutter'
Plug 'preservim/nerdcommenter'
Plug 'sbdchd/neoformat'
Plug 'purescript-contrib/purescript-vim'
Plug 'barboza/vim-cucumber-string-highlight'
Plug 'vmchale/dhall-vim'
Plug 'lotabout/skim', { 'dir': '~/.skim', 'do': './install' }
Plug 'lotabout/skim.vim'
Plug 'ryanoasis/vim-devicons'
Plug 'tpope/vim-surround'
Plug 'easymotion/vim-easymotion'
Plug 'aykamko/vim-easymotion-segments'
Plug 'kana/vim-textobj-user'
Plug 'julian/vim-textobj-variable-segment'
Plug 'maxbrunsfeld/vim-yankstack'
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-repeat'
Plug 'aklt/plantuml-syntax'
Plug 'tyru/open-browser.vim'
Plug 'weirongxu/plantuml-previewer.vim'
call plug#end()

" lsp specific config
lua << EOF
  local nvim_lsp = require('lspconfig')

  local on_attach = function(_, bufnr)
	-- vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

	-- mappings
	local opts = { noremap=true, silent=true }
	vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gD', '<Cmd>lua vim.lsp.buf.declaration()<CR>', opts)
	vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gd', '<Cmd>lua vim.lsp.buf.definition()<CR>', opts)
	vim.api.nvim_buf_set_keymap(bufnr, 'n', 'K', '<Cmd>lua vim.lsp.buf.hover()<CR>', opts)
	vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
	vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>k', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
	vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
	vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
	vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
	vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>e', '<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>', opts)
	vim.api.nvim_buf_set_keymap(bufnr, 'n', 'g0', '<Cmd>lua vim.lsp.buf.document_symbol()<CR>', opts)
	vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gW', '<Cmd>lua vim.lsp.buf.workspace_symbol()<CR>', opts)
        vim.api.nvim_buf_set_keymap(bufnr, 'n', 'ga', '<Cmd>lua vim.lsp.buf.code_action()<CR>', opts)
  end

  local configs = require('lspconfig/configs')
  configs.dhall_lsp = {
    default_config = {
      cmd = {'dhall-lsp-server'};
      filetypes = {'dhall'};
      root_dir = function(fname)
        return nvim_lsp.util.find_git_ancestor(fname) or vim.loop.os_homedir()
      end;
      settings = {};
    };
  };

  nvim_lsp.rust_analyzer.setup {
	on_attach = on_attach,
	settings = {
            ['rust-analyzer'] = {
	        diagnostics = {
                  disabled = {"unresolved-proc-macro"},
                  enableExperimental = false,
                },
	    },
	},
  }

  local servers = {'tsserver', 'cssls', 'html', 'purescriptls', 'dhall_lsp', 'hls', 'pylsp'}
  for _, lsp in ipairs(servers) do
	nvim_lsp[lsp].setup {
	on_attach = on_attach,
	}
  end


EOF

  let g:python3_host_prog = '~/.pyenv/versions/neovim3/bin/python'

  " Enable deoplete autocompletion
  let g:deoplete#enable_at_startup = 1

  " customise deoplete
  " maximum candidate window length
  call deoplete#custom#source('_', 'max_menu_width', 80)

  " Press Tab to scroll _down_ a list of auto-completions
  let g:SuperTabDefaultCompletionType = "<c-n>"

  " using autoformat on write
  autocmd BufWritePre * Neoformat

  " this is for the unfortunate case that prettier config sets 'endOfLine:
  " crlf'
  autocmd FileType typescript*,yaml,xml,javascript,json,scss setlocal formatprg=prettier\ --end-of-line\ lf\ --stdin-filepath\ %
  let g:neoformat_try_formatprg = 1

  set ignorecase

  let mapleader=','
  autocmd FileType rust nnoremap <leader>c :!cargo clippy

  let g:NERDTreeWinSize = 50
  nnoremap <silent> <leader>t    :NERDTreeToggle<CR>
  nnoremap <silent> <leader>v    :NERDTreeFind<CR>

  autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

  let g:airline#extensions#tabline#enabled = 1
  let g:airline#extensions#tabline#buffer_nr_show = 1
  let g:airline_powerline_fonts = 1

  " unimpaired
  nmap < [
  nmap > ]
  omap < [
  omap > ]
  xmap < [
  xmap > ]

  nmap <silent> <q <Plug>unimpairedQPrevious :call repeat#set("\<Plug>unimpairedQPrevious",v:count)<CR>
  nmap <silent> >q <Plug>unimpairedQNext :call repeat#set("\<Plug>unimpairedQNext",v:count)<CR>

